# Belt

Welcome to Belt; the perfect init environment for my own projects. Chances are, it’ll work nicely for some of yours, too!

## What do I get?
Here’s a quick run-down of the generated file/folder structure:

```
- assets/
  - css/
  - images/
    - src/
  - js/
    - src/
      - main.js
  - scss/
    - modules/
      - _all.scss
      - _base.scss
      - _code.scss
      - _content.scss
      - _footer.scss
      - _header.scss
    - partials/
      - _all.scss
      - _animations.scss
      - _debug.scss
      - _forms.scss
      - _grid.scss
      - _helpers.scss
      - _images.scss
      - _layout.scss
      - _mixins.scss
      - _objects.scss
      - _reset.scss
      - _tables.scss
      - _typography.scss
      - _variables.scss
    - style.scss
  - vendor/
- .bowerrc
- bower.json
- config.rb
- Gruntfile.js
- index.html
- README.md

```

Let’s go over some of the nitty gritty.

- `assets/` contains—surprise, surprise—assets for your project. When running, Grunt will watch for changes in `images` and `scss` to optimise any SVGs found in `images/src/`, create PNGs from them, and compile your Sass files to CSS. Phew!
- `assets/scss/` is kind of a treasure trove, but hopefully all the SCSS files in there are well-written enough for you to understand. Consider it homework.
- `.bowerrc` just tells Bower where to install the things your project needs (which, by default, is `assets/vendor/`).
- `bower.json` contains the components needed for the project (by default, just jQuery).
- `config.rb` contains configuration options for Compass and Sass.
- `Gruntfile.js` is the workhorse, with rules for watching and processing files. It compiles Sass, optimises and converts SVGs, concatenates JS, adds prefixes to CSS, adds `rem` fallbacks to CSS, and compresses CSS.
- `index.html` is just there for demonstrative purposes.
- `README.md` is generated from the project title and description.

## How do I use it?
Glad you asked!

1. Install grunt-init: `npm install -g grunt-init`
2. Clone Belt into your grunt-init template directory: `git clone https://daneden@bitbucket.org/daneden/belt.git ~/.grunt-init/belt`
3. In your new, empty project directory, run grunt init: `grunt-init belt`
4. Finally, grab the dependencies and get Grunt running: `npm install && bower install && grunt`

That’s it. Easy, right?

## I need help!
Don’t we all? I can help you with Belt if you have any problems. I’d suggest opening [an issue](https://bitbucket.org/daneden/belt/issues?status=new&status=open) so that the wider community can also check if they have the same problem, and help little old me to fix it.

## Who are you?
I’m [Daniel Eden](http://daneden.me). I make tools for people like you and me.
